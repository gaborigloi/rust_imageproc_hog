extern crate image;
extern crate imageproc;

use std::path::Path;
use std::io::Cursor;

use image::imageops::grayscale;
use image::GenericImage;

use imageproc::hog::hog;
use imageproc::hog::render_hist_grid;
use imageproc::hog::cell_histograms;
use imageproc::hog::HogOptions;
use imageproc::hog::HogSpec;
use imageproc::multiarray::View3d;

fn main() {
    let img = image::load(Cursor::new(&include_bytes!("../resources/hogtest2.png")[..]),
                          image::JPEG).unwrap();
    let img_grayscale = grayscale(&img);
    let hog_options = HogOptions::new(18, true, 8, 2, 1);
    let hog_result = hog(&img_grayscale, hog_options);
    println!("{}", hog_result.is_ok());

    let hog_spec = HogSpec::from_options(img_grayscale.width(), img_grayscale.height(), hog_options).unwrap();
    let mut hog_descriptor = cell_histograms(&img_grayscale, hog_spec);
    let grid = render_hist_grid(20, &hog_descriptor.view_mut(), true);
    let _ = grid.save(Path::new("out.png"));
}
